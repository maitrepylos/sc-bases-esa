JavaScript : les Objets 
=======================

##### JavaScript orienté objet

Nous allons voir comment :

-   Créer et utiliser des objets,
-   Les fonctions de type constructeurs pour construire des objets,
-   Les objets built-in de JavaScript et leur utilité.


##### Des tableaux aux objets...
```javascript
let myarr = ['red', 'blue', 'yellow', 'purple'];
myarr;
 //["red", "blue", "yellow", "purple"]
myarr[0]
//"red"
myarr[3]
//"purple"
``` 
![](img/tableau.jpg)


##### En JavasScript un objet = un tableau dont on définit les clés !
```javascript
    let heros = {
      race: 'Tortue',
      metier: 'Ninja'
    }; 
```        
Nom de la variable qui contient l'objet = heros,
Au lieu de \[ et \] on utilise { et },
Les éléments de l'objet (ses propriétés) sont séparés par une virgule,
Les paires de clés/valeurs sont séparées par : comme dans race:'Tortue'


##### Syntaxe de déclaration des propriétés
On peut mettre des simple ou double quotes autour du nom de la
propriété, ou rien du tout :
```javascript
    let louis = {age: 40};
    let louis = {"age": 40};
    let louis = {'age': 40};       
```
Dans certains cas on est obligé de mettre des quotes autour du nom de la
propriété :
-   Mot réservé de JavaScript,
-   ccontient des espaces ou des caractères spéciaux,
-   commence par un chiffre


##### Exemple qui utilise les trois possibilités
```javascript
    let objetBizarre = {
      age: 1,
      'oui ou non': 'oui',
      '!@#$%^&*': true
    };
```


##### Eléments, propriétés, méthodes
Pour des tableaux on parle d'éléments,
Pour des objets on parle de propriétés
Une propriété peut aussi valoir une fonction, dans ce cas on parle
de méthode.
```javascript
    let medor = {
      nom: 'Benji',
      parle: function(){
        alert('Ouah, Ouah!');
      }
    };
```


##### Tables de hashage et tableaux associatifs
Dans certains langages de programmation (Java par ex) on distingue :
1.  Un tableau normal, donc les indexs sont des valeurs numériques,
2.  Un tableau associatif dont les clés sont des Strings ou des objets
    (ex: HashMap en Java)

JavaScript utilise les tableaux pour le cas 1, et des objets pour le cas
2


##### Accès aux propriétés d'un objet
```javascript
    let medor = {
      nom: 'Benji',
      parle: function(){
        alert('Ouah, Ouah!');
      }
    };
```     
Deux syntaxes possibles :
1.  medor\['nom'\]
2.  medor.nom (à utiliser si possible)


##### Accès aux propriétés
```javascript
    let heros = {
      race: 'Tortue',
      metier: 'Ninja'
    };
     heros.race;
       "Tortue"
     heros['race'];
       "Tortue"
     heros.age;
       "age is Undefined"
```


##### Un objet peut contenir un autre objet
```javascript
    let book = {
      name: 'Catch-22',
      published: 1961,
      author: {
        firstname: 'Joseph',
        lastname: 'Heller'
      }
    };
     book.author.firstname    // à préférer
           "Joseph"
     book['author']['lastname']
           "Heller"
     book.author['lastname']  // on peut mélanger...
           "Heller"
     book['author'].lastname
           "Heller"
```


##### Autre cas classique où le nom d'une propriété est dans une variable
```javascript
     let key = 'firstname';
     book.author[key];
        "Joseph"
```
Dans ce cas on est obligé d'utiliser la syntaxe avec \[ et \]...


##### Appel de méthodes
Une méthode étant une propriété on peut utiliser l'opérateur . ou les
crochets
```javascript
    let heros = {
      race: 'Tortue',
      metier: 'Ninja',
      parle: function() {
        return 'Je suis un ' + heros.metier;
      }
    }
     heros.parle();       // recommandée comme syntaxe
           "Je suis un Ninja"
    heros['parle']();     // peu utilisé car moins Java-esque
           "Je suis un Ninja"
```


##### Modification de propriétés
Contrairement à Java, il est possible en JavaScript d'ajouter ou de
supprimer des propriétés une fois l'objet créé.
```javascript
     let heros = {};   // Objet sans propriétés
     typeof heros.metier
            "Undefined"
     heros.race = 'Tortue';
     heros.nom = 'Leonardo';
     heros.getNom = function() {return heros.nom;};
     hero.getNom();
           "Leonardo"
     delete heros.nom;
            true
     hero.getNom();
             reference to undefined property heros.nom
```


##### Utilisation de this
Dans l'exemple précédent le code de la méthode getNom() faisait
référence au nom de l'objet, il vaut mieux utiliser le ot clé this.
```javascript
    let heros1 = {
      nom: 'Rafaelo',
      getNom: function() {
        return this.nom;
      }
    }
     heros1.getNom();
           "Rafaelo"     
```        
this correspond à "l'objet courant". Le code est plus portable si on
affecte cet objet à une autre variable....


##### Constructeurs en JavaScript
Un exemple suffit à comprendre le principe (remarquez qu'on nomme la
fonction comme une classe Java):
```javascript
    function Heros() {
      this.metier = 'Ninja';
    }       
```        
On peut maintenant utiliser l'opérateur new :
```javascript
     let monHero = new Heros();
    typeof monHero
          "object"
     monHero.metier;
           "Ninja"
```


##### Constructeur avec paramètres
```javascript
    function Heros(nom) {
      this.nom = nom;
      this.metier = 'Ninja';
      this.decrisToi = function() {
        return "Je suis " + this.nom + ", je suis un " + this.metier;
      }
    }
     let h1 = new Heros('Michelangelo');
     let h2 = new Heros('Donatello');
     h1.decrisToi();
           "Je suis Michelangelo, je suis un Ninja"
     h2.decrisToi();
           "Je suis Donatello, je suis un Ninja"
```        
Note : cela commence un peu à ressembler à des classes Java...non ?


##### Remarques sur les constructeurs
Donner un nom de fonction commençant par une majuscule permet de
distinguer les constructeurs !
```javascript
     let monHero = new Heros();
    typeof monHero
          "object"        
```        
Mais :
```javascript
let monHero = Heros();
typeof monHero
"undefined"
```        
Normal, car dans ce cas monHero vaut la valeur de retour de Heros() qui
ne renvoie rien !
Dans ce cas, le this à l'intérieur de la fonctin Heros() se réfère à ce
que l'on appelle l'objet global, qui contient tous les autres.
