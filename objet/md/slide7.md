##### L'objet prédéfini Math


Particularité : on ne peut pas faire de "new Math()", mais l'objet Math
possède de nombreuses propriétés et fonctions utiles pour les
expressions arithmétiques.
```javascript
     Math.PI
           3.141592653589793
     Math.SQRT2      // racine carrée
           1.4142135623730951
     Math.E          // Constante d'Euler
           2.718281828459045
     Math.LN2        // Log naturel de 2
           0.6931471805599453
    Natural logarithm of 10:
     Math.LN10       // Log naturel de 10
           2.302585092994046
```


##### Math : nombres aléatoires
Générer un nombre réel aléatoire entre 0 et 1 :
```javascript
     Math.random() 
      0.3649461670235814         
```
Entre 0 et 100
```javascript
     100 * Math.random()
```


Entre min et max : ((max - min) \* Math.random()) + min
```javascript
     8 * Math.random() + 2 // nombre entre 2 et 10
     9.175650496668485
```


##### Math et arrondis : round(), ceil(), floor()
Pour arrondir au plus proche, à l'entier supérieur ou inférieur.
Par ex, pour obtenir soit 0 soit 1 :
```javascript
     Math.round(Math.random())
```
Il y a aussi Math.min() 
Exemple pour restreindre une valeurentre 1 et 12 :
```javascript
     Math.min(Math.max(1, input), 12)
```


##### Math et opérations mathématiques : sin(), cos(), tan(), atan(), pow(), sqrt()
```javascript
     Math.pow(2, 8)    // 2 puissance 8
           256
     Math.sqrt(9)      // Racine carrée de 9
           3
```


##### L'objet prédéfini Date
Date() est un constructeur, qui prend plusieurs arguments :
-   Rien : construit la date courante,
-   Une chaîne de caractère qui encode une date,
-   Un ensemble de valeurs numériques séparées par une virgule pour
    mois, jour, heure, etc.
-   Un "timestamp" Unix (nb de millisecondes écoulées depuis 1970


```javascript
     new Date()
```
Tue Jan 08 2008 01:10:42 GMT-0800 (Pacific Standard Time)
Remarque : la valeur retournée est en fait un objet de type Date, ce qui
s'affiche est l'appel à toString() sur cet objet.


##### Date : exemples
```javascript
     new Date('2009 11 12')
          Thu Nov 12 2009 00:00:00 GMT-0800 (Pacific Standard Time)
     new Date('1 1 2012')
          Sun Jan 01 2012 00:00:00 GMT-0800 (Pacific Standard Time)
     new Date('1 mar 2012 5:30')
          Thu Mar 01 2012 05:30:00 GMT-0800 (Pacific Standard Time)
```


On peut passer aussi des paramètres numériques pour, dans l'ordre :
année, mois (0-11), jour (1-31), heure (0-23), minutes (0-59), secondes
(0-59), millisecondes (0-999). On n'est pas obligé de tout passer mais
ce sera toujours dans cet ordre.


##### Date : exemple de construction avec paramètres numériques
```javascript
     new Date(2008, 0, 1, 17, 05, 03, 120)
           Tue Jan 01 2008 17:05:03 GMT-0800 (Pacific Standard Time)
     new Date(2008, 0, 1, 17)
           Tue Jan 01 2008 17:00:00 GMT-0800 (Pacific Standard Time)
     new Date(2008, 1, 28)  // 1 c'est Février ! Ca commence à zéro !
         Thu Feb 28 2008 00:00:00 GMT-0800 (Pacific Standard Time)
     new Date(2008, 1, 29)
           Fri Feb 29 2008 00:00:00 GMT-0800 (Pacific Standard Time)
     new Date(2008, 1, 30) // Pas de 30 février - ça donne premier Mars !
           Sat Mar 01 2008 00:00:00 GMT-0800 (Pacific Standard Time)
     new Date(2008, 11, 31)
          Wed Dec 31 2008 00:00:00 GMT-0800 (Pacific Standard Time)
     new Date(2008, 11, 32) // 32 Dec - 1er Janvier !
          Thu Jan 01 2009 00:00:00 GMT-0800 (Pacific Standard Time)
``` 


##### Date : suite...
On peut construire la date avec un timestamp Unix (nb millisecondes
depuis 1970)
```javascript
     new Date(1199865795109)
```
Wed Jan 09 2008 00:03:15 GMT-0800 (Pacific Standard Time)


L'appel de Date() sans "new" renvoie la date courante sous forme de
chaine de caractères. Peu importe que l'on passe des paramètres.
```javascript
     Date()
           "Thu Jan 11 2012 18:15:47 GMT-0800 (Pacific Standard Time)"
```


##### Date : méthodes utiles !
```javascript
    // Construction
     let d = new Date();
     d.toString();
           "Wed Jan 09 2008 00:26:39 GMT-0800 (Pacific Standard Time)"
    // Chenger le mois pour Mars
     d.setMonth(2);
           1205051199562
     d.toString();
           "Sun Mar 09 2008 00:26:39 GMT-0800 (Pacific Standard Time)"
    // Récupérer le mois
     d.getMonth();
           2
```


##### Date : deux méthodes qui sont des propriétés de l'objet Date
Date.parse() transforme une chaine de
caractère représentant une date en timestamp :
```javascript
     Date.parse('Jan 1, 2008')
           1199174400000
```
Date.UTC() transforme des paramètres
numériques représentant une date en timestamp :
```javascript
     Date.UTC(2008, 0, 1)
           1199145600000
```


##### Date : jouons avec un anniversaire !
Quel jour est l'anniversaire de John en 2012 ?
```javascript
     let d = new Date(2012, 5, 20); // Le 20 Juin !
     d.getDay();
           3
    Dimanche = 0 donc 3 = mercredi, vérifions !
     d.toDateString();
           "Wed Jun 20 2012" // C'est juste !
```


Combien de fois l'anniversaire de John sera un dimanche, un lundi,
etc... entre 2012 et 3012 :
```javascript
    let joursAnniv = [0,0,0,0,0,0,0];

    for (let an = 2012; an < 3012; an++) {
      joursAnniv[new Date(an, 5, 20).getDay()]++;
    }
     joursAnniv;
           [139, 145, 139, 146, 143, 143, 145]
    143 Vendredis et 145 Samedis ! On pourra faire la fête !
```