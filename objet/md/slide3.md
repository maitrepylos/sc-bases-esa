##### L'objet global
Il est temps de dire la vérité :

-   Le code JavaScript est exécuté par un environnement (la plupart du
    temps le navigateur Web, ou plus rarement un Serveur),
-   Cet environnement définit un objet global,
-   Les variables globales sont les propriétés de cet environnement, de
    même que les fonctions prédéfinies.
Cas où l'environnement est un navigateur web :
-   L'objet global s'appelle window


##### Continuons sur l'objet global...
```javascript
 let a = 1;
 a
 1
 window.a
 1
 window['a']
 1
function Heros(nom) {this.nom = nom;}
// appel sans new, this = objet global
let h = Heros('Leonardo');
typeof h
 "undefined"
 typeof h.nom
 h has no properties h has no properties
 nom
 "Leonardo"
 window.nom
 "Leonardo"
// Si on oublie pas le new :
     let h2 = new Heros('Michelangelo'); // On a pas oublié le new !
     typeof h2
          "object"
     h2.nom
          "Michelangelo"
```


###### Les fonctions prédéfinies sont des méthodes de l'objet global window :
```javascript
     parseInt('101 dalmatiens')
           101
     window.parseInt('101 dalmatiens')
           101
```           
           
###### La propriété constructor

Quand un objet est créé on lui donne en coulisse une propriété nommée
"constructor" :
```javascript
     h2.constructor
           Heros(nom)
     typeof a.constructor
           "function"
```



Puisque ce constructeur est une fonction, on peut l'appeler :
```javascript
     let h3 = new h2.constructor('Rafaello'); // construit moi comme h2
     h3.nom;
           "Rafaello"
        
```


##### La propriété constructor, suite...

Dans le cas où on construit un objet sous sa forme littérale :
```javascript
     let o = {};
     o.constructor;
           Object()
     typeof o.constructor;
           "function"   
 ``` 
 Object() est le constructeur built-in de JavaScript. Détails viendront
plus loin...


##### L'opérateur instanceof
instanceof permet de tester avec quel
constructeur un objet a été créé
```javascript
     function Heros(){}
     let h = new Heros();
     let o = {};
     h instanceof Herso;
           true
     h instanceof Object;
           false             // Pas comme en Java !
     o instanceof Object;
           true
 ```       