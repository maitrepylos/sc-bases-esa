##### Array
Array() est une fonction built-in que l'on peut utiliser pour construire
des tableaux :
```javascript
     let a = new Array(); // équivalent à let a = [];
     let b = new Array(1,2,3,'quatre');
     b;
           [1, 2, 3, "quatre"]
```


##### Piège : si un seul paramètre alors c'est la taille du tableau:
```javascript
     let a2 = new Array(5);
     a2;
           [undefined, undefined, undefined, undefined, undefined]
```


##### Array et objets...
Dans les exemples précédents si on créée des tableaux avec Array() ce
sont des objets :
```javascript
     typeof a;
           "object"
```


Et on peut bénéficier des méthodes de Object
```javascript
     a.toString();
           "1,2,3,quatre"
     a.valueOf()
           [1, 2, 3, "quatre"]
     a.constructor
           Array()
```


##### Compléments sur les tableaux
Les tableaux sont en fait des objets mais spéciaux :

-   Le nom de leurs propriétés sont des valeurs numériques partant de 0,
-   Ils ont une propriété length qui donne leur longueur,
-   Ils possèdent d'autres propriétés built-in en plus de celles
    héritées de Object
```javascript
     let a = [], o = {};
     a.length
           0
     typeof a.length
           "number"
     typeof o.length
           "undefined"
```


##### Tableaux = objet ou pas ?
Hmmm... on peut faire de jolies horreurs quand même :
```javascript
     a[0] = 1; o[0] = 1;
     a.prop = 2; o.prop = 2;
     a.length
          1
```
La propriété length ne "compte" que les propriétés numériques !


##### length peut être modifée !
Si plus grand que la taille du tableau
```javascript
     a.length = 5 // peut agrandir le tableau
           5
     a
           [1, undefined, undefined, undefined, undefined]
```
Si plus petit, réduit le tableau
```javascript
     a.length = 2;
           2
     a
           [1, undefined]
```


##### Méthodes utiles sur les tableaux
Les plus courantes : sort(), join(), slice(),
splice(), push() et pop()
```javascript
     let a = [3, 5, 1, 7, 'test'];
     a.push('new') // ajout en fin et renvoie length
           6
     a
           [3, 5, 1, 7, "test", "new"]
     a.pop() // retire le dernier élément et le renvoie
           "new"
     a
           [3, 5, 1, 7, "test"]
     let b = a.sort();
     b
           [1, 3, 5, 7, "test"]
     a
           [1, 3, 5, 7, "test"]
     a.join(' et ');
        "1 et 3 et 5 et 7 et test"
```


##### Suite des fonctions utiles sur les tableaux
slice() renvoie un sous-tableau sans
modifier l'original.
```javascript
     a
           [1, 3, 5, 7, "test"]
     b = a.slice(1, 3); // éléments 1 et 2
        [3, 5]
     b = a.slice(0, 1); // élément 0
        [1]
     b = a.slice(0, 2); // éléments 0 et 1
        [1, 3]
     a
       [1, 3, 5, 7, "test"]
```


##### Suite des fonctions utiles sur les tableaux
splice() modifie le tableau en enlevant
une tranche, éventuellement en ajoutant aussi de nouveaux éléments.
Les deux premiers paramètres sont les indices de début et fin, les
autres paramètres sont les éléments à insérer à la place de la tranche
enlevée.
```javascript
     a
       [1, 3, 5, 7, "test"]
     b = a.splice(1, 2, 100, 101, 102);
       [3, 5]
     a
       [1, 100, 101, 102, 7, "test"]
     a.splice(1, 3)
           [100, 101, 102]
     a
           [1, 7, "test"]
```