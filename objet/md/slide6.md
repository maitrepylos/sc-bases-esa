##### Function
Les fonctions sont des objets en JavaScript, elles ont toutes un
constructeur intitulé Function()
```javascript
     function myfunc(a){return a;}
     myfunc.constructor
        Function()
        
```
Les fonctions ont aussi une propriété length qui vaut le nombre de
paramètres :
```javascript
     function myfunc(a, b, c){return true;}
     myfunc.length
           3
```


##### Function, suite...
Elles possèdent aussi une propriété caller
qui renvoie la fonction appelante :
```javascript
     function A(){return A.caller;}
     function B(){return A();}
     B();
           B()
```       
Si A est appelée depuis l'espace global, l'appelant vaut null :
```javascript
     A();
          null
```


##### Function, propriété "prototype"
prototype est la propriété la plus
importante des fonctions JavaScript
Elle contient un objet,
Elle n'est utile que lorsqu'on utilise une fonction comme un
constructeur, par ex: let heros = new Heros('Michel');
Tous les objets créés par cette fonction/constructeur ont une référence
vers la propriété prototype de leur constructeur, et considèrent les
propriétés de cet objet prototype comme les leurs.
Voyons quelques exemples...


##### La propriété prototype des fonctions
```javascript
    let unObjet = {
      nom: 'Ninja',
      parle: function(){
        return 'Je suis : ' + this.nom;
      }
    }
 ```
Créons une fonction vide, ah, elle a un prototype !
```javascript
     function F(){}
     typeof F.prototype
           "object"
```


##### La propriété prototype des fonctions
Modifions la valeur de cet objet prototype :
```javascript
     F.prototype = unObjet;
```
Et utilisons F comme un constructeur !
```javascript
     let obj = new F();
     obj.nom
           "Ninja"
     obj.parle()
           "Je suis Ninja"
```
Intéressant non ? Nous y reviendrons...


##### Méthodes de l'objet Function : toString()
Function est un descendant de Object, il hérite donc des méthodes de ce
dernier, comme toString()
toString() sur une fonction : renvoie le
code de la fonction sous forme de String
```javascript
     function myfunc(a, b, c) {return a + b + c;}
     myfunc.toString()
           "function myfunc(a, b, c) { 
               return a + b + c; 
            }"
```


##### toString() et fonctions natives
SI on appelle toString sur une fonction native on obtient \[native
code\] :
```javascript
     eval.toString()
        "function eval() {
            [native code]
        }"
```


##### Méthodes de l'objet Function : call() et apply()
Permettent d'emprunter des méthodes à d'autres objets et de les appeler
:
```javascript
    let unObjet = {
      nom: 'Ninja',
      parlerA: function(interlocuteur){
        return 'Hello ' + interlocuteur + ', je suis un ' + this.nom;
      }
    }
     unObjet.parlerA('Michel');
      "Hello Michel, je suis un Ninja"       

     unAutreObjet = {nom: 'Gourou JavaScript'};
```


##### Appropriation de méthode avec call()
UnAutreObjet aime tellement la méthode parlerA() de UnObjet qu'il veut
l'appeler :
```javascript
     unObjet.parlerA.call(unAutreObjet, 'Michel');
           "Hello Michel, je suis un Gourou JavaScript"        
```
La fonction parlerA() de unObjet a été appelée avec un "this" différent
correspondant à unAutreObjet.
On fait cela en appelant la méthode call() d'une méthode pour changer
son "this" !


##### call()... suite...
Si la méthode a plus de paramètres, on les ajoute :
```javascript
    unObjet.uneMethode.call(unAutreObjet, 'a', 'b', 'c');        
```
Si on ne passe pas d'objet ou si on passe null comme premier paramètre,
c'est l'objet global qui correspondra à "this" dans le corps de la
méthode.


##### call() et apply()
apply() est équivalent à call() si ce n'est que les paramètres sont
passés dans un tableau :
```javascript
    unObjet.uneMethode.apply(unAutreObjet, ['a', 'b', 'c']);
    unObjet.uneMethode.call(unAutreObjet, 'a', 'b', 'c');       
```  
Les deux lignes précédentes sont équivalentes.
Avec l'exemple précédent :
```javascript
     unObjet.parlerA.apply(unAutreObjet, ['Michel']);
           "Hello Michel, je suis un Gourou JavaScript"
```


##### arguments.callee : une propriété intéressante !
arguments est comme un tableau (propriété "length") mais ce n'en est pas
un, (pas de slice() et sort() par exemple).
arguments.callee : référence la fonction
elle-même.
Utile pour faire des fonctions anonymes récursives :
```javascript
    (function(count){
        if (count < 5) {
          alert(count);
          arguments.callee(++count);
        }
      })(1);
``` 
L'exemple précédent affiche dans une alerte "1", puis "2", "3" et "4".


##### L'objet prédéfini Boolean
Comme en Java, c'est un objet qui wrappe le type prédéfini boolean.
On utilise la méthode valueOf() pour avoir une variable du type
prédéfini équivalent.
```javascript
     let b = new Boolean();
     typeof b
           "object"
     b.valueOf()
         false
     typeof b.valueOf()
           "boolean"        
```


##### Utilité de l'objet Boolean : convertir des objets en boolean !
On appelle pour cela Boolean(...) sans new !
```javascript
     Boolean("test")
           true
     Boolean("")
           false
     Boolean({})
           true
```
N'importe quel objet, même vide, se convertit en "true".


##### Un objet se convertit toujours en boolean dont la valeur est true !
```javascript
     let b1 = new Boolean(true)
     b1.valueOf()
           true
     let b2 = new Boolean(false)
     b2.valueOf()
           false       

     Boolean(b1)
           true
     Boolean(b2)
           true
```


##### L'objet prédéfini Number
Wrapper sur le type prédéfini "number".
Number() sans "new" sert à convertir en "number", comme parseInt() ou
parseFloat()
```javascript
     let n = Number('12.12');
     n
           12.12
     typeof n
           "number"
     let n = new Number('12.12');
     typeof n
           "object"
```


##### Propriétés prédéfinies (non modifiables) de Number()
Comme en JavaScript les fonctions sont des
objets, Number() a des propriétés intéressantes :
```javascript
     Number.MAX_VALUE
           1.7976931348623157e+308
     Number.MIN_VALUE
           5e-324
     Number.POSITIVE_INFINITY
           Infinity
     Number.NEGATIVE_INFINITY
           -Infinity
     Number.NaN
           NaN
```


##### Propriétés des objets de type Number
Conversions : toFixed(), toExponential(), toString()
```javascript
     let n = new Number(123.456)
     n.toFixed(1)
           "123.5"
     (12345).toExponential()
           "1.2345e+4"
     let n = new Number(255);
     n.toString();
           "255"
     n.toString(10);
           "255"
     n.toString(16);
           "ff"
     (3).toString(2);
           "11"
     (3).toString(10);
           "3"
```


##### L'objet prédéfini String
new String() construit un objet, on peut utiliser les méthodes et
propriétés prédéfinies de String.
```javascript
     let primitive = 'Hello';
     typeof primitive;
         "string"
     let obj = new String('world');
     typeof obj;
          "object"
     obj[0]
          "w"
     obj[4]
          "d"
     obj.length
          5
```


##### String, suite...
On peut une "string" à partir d'un objet de type String :
```javascript
     obj.valueOf()
           "world"
     obj.toString()
           "world"
     obj + ""
           "world"
```


JavaScript permet d'utiliser certains aspects des String sur des
"string" :
```javascript
     "potato".length
           6
     "tomato"[0]
           "t"
     "potato"["potato".length - 1]
           "o"
``` 


##### Conversion d'objets en "string" (type prédéfini)
On appelle String() sans new, comme pour Boolean().
```javascript
     String(1)
          "1"
     String({p: 1})
          "[object Object]"
     String([1,2,3])
          "1,2,3"       
```
Ces exemples reviennent à appeler toString() sur les objets en
paramètre.


##### Méthodes intéressantes des objets de type String
```javascript
     let s = new String("Couch potato");
     s.toUpperCase()
           "COUCH POTATO"
     s.toLowerCase()
           "couch potato"
     s.charAt(0);
           "C"
     s[0]
           "C"
     s.charAt(101)
           ""
     s.indexOf('o')
           1
     s.indexOf('o', 2); // 2 est l'index de départ !
           7
```


##### Méthodes de String, suite...
```javascript
     let s = new String("Couch potato");
     s.lastIndexOf('o')
           11
     s.indexOf('Couch'); // case sensitive
           0
     s.indexOf('couch');  // si pas trouvé renvoie -1
           -1
     s.toLowerCase().indexOf('couch')
           0
```


##### String : Piège avec if() et indexOf()
Il ne faut pas tester un index avec un if car si l'index est zéro (début de
chaine), alors on teste "false" !

Pas bon :
```javascript
    if (s.indexOf('Couch')) {...}
```       
Bon :
```javascript
    if (s.indexOf('Couch') !== -1) {...}
```


##### String : slice() et substring()
Permettent d'extraire une sous-chaine, on passe la position de début et
la position de fin en paramètre :
```javascript
     let s = new String("Couch potato");        
     s.slice(1, 5)
           "ouch"
     s.substring(1, 5)
           "ouch"        
```


Différence si le dernier paramètre est négatif :
```javascript
     s.slice(1, -1); // équivalent à s.slice(1, s.length -1)
           "ouch potat"
     s.substring(1, -1); // équivalent à s.substring(1,0);
           "C"
```


##### String : méthode split(), join(), concat()
Split renvoie un tableau de chaine, le paramètre est un séparateur,
join() construit une String à partir d'un tableau de chaines :
```javascript
     let s = new String("Couch potato");
     s.split(" ")
         ["Couch", "potato"]
     s.split(' ').join(' ');
           "Couch potato"
     s.concat("es"); // équivalent à s = s + "es;
           "Couch potatoes"
     s.valueOf();      
           "Couch potato"        
``` 
Aucune de ces méthodes ne modifie la chaine de départ.