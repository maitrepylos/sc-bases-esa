##### Fonctions qui retournent des objets (factories)
En plus de la création littérale et de la création par new, on peut
aussi créer des objets à l'aide de fonctions qui en renvoient :
```javascript
    function factory(nom) {
      return {
        nom: nom
      };
    }
     let o = factory('Michel');
     o.nom
           "Michel"
     o.constructor
           Object()
```


##### Passage d'objets en paramètres, copie d'objets
Par défaut, toute manipulation d'objet se fait par référence :
```javascript
     let original = {valeur: 1};
     let copie = original;
     copie.valeur
           1
     copy.valeur = 100;
           100
     original.valeur
           100        
```  
Les deux variables copie et original pointent en fait sur le même
objets, ce sont deux références au même
objet.


##### Comparer des objets
Comparer deux objets ne donnera true que s'ils ont la même référence.
Deux objets à priori identiques ne seront pas égaux s'ils n'ont pas la
même référence
```javascript
     let fido  = {race: 'chien'};
     let benji = {race: 'chien'};
     benji === fido
       false
     benji == fido
       false
     let monChien = benji;
     mydog === benji
           true
     mydog === fido
         false
```


##### Objets JavaScript built-in
Objets "wrappers" : Object, Array, Function, Boolean, Number, et String,
ils correspondent aux types prédéfinis.
Objets utilitaires : Math, Date, RegExp
Objets pour le debug : Error, etc.


##### Object
C'est le "père" de tous les objets JavaScript que vous créez. Tous vos
objets "héritent" de celui-ci, si on peut parler comme ça...
Ces deux lignes sont équivalentes :
```javascript
     let o = {};
     let o = new Object();        
````
On bénéficie de méthodes par défauts comme toString() qui convertit
l'objet en String:
```javascript
       o.toString()
         "[object Object]"
```


##### toString() est assez similaire à son usage Java

     alert(o);
     alert(o.toString()); // équivalent à la ligne précédente
     "L'objet en String : " + o // toString() implicite !
           "L'objet en String : [object Object]"
        

##### valueOf() renvoie la valeur d'un objet

     o.valueOf() === o
          true
