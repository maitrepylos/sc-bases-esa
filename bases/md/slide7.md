##### Commentaires dans le code

Similaires à Java : à l'aide de // ou de /*......*/
```javascript
// début de ligne
let a = 1; // ailleurs sur une ligne
/*
Un commentaire
sur plusieurs lignes
*/
let b = 2;
```