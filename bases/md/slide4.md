#### Strings

Une chaine = entre simple ou double quotes "Hello" ou 'hello'...

```javascript
let s = "quelques caractères";
 typeof s;
       "string"
 let s = 'quelques caractères et des nombres 123 5.87';
 typeof s;
       "string"
 let s = '1';
 typeof s;
       "string"
 let s = ""; typeof s;
    "string"
```

--

#### Concaténation de chaines...

Comme en Java, l'opérateur "+" permet de concaténer des chaines...

```javascript
 let s1 = "un"; let s2 = "deux"; let s = s1 + s2; s;
  "undeux"
 typeof s;
  "string"
```

--

#### Conversion de chaines de caractères...

Un nombre sous forme de String dans une expression arithmétique est converti en Number, sauf si la formule est une addition pure.

```javascript
 let s = '1'; s = 3 * s; typeof s;
      "number"
 s
      3
 let s = '1'; s++; typeof s;
      "number"
 s
      2
 let s = "100"; typeof s;
     "string"
 s = s * 1;
     100
 typeof s;
     "number"
 let d = '101 dalmatiens';
 d * 1
     NaN
```

--

#### Conversion de nombre en String, astuce...

On concatène avec une chaine vide, en début d'expression

```javascript
 let n = 1;
 typeof n;
      "number"
 n = "" + n;
      "1"
 typeof n;
      "string"
````

--

#### Caractère spéciaux, le "\" !

```javascript
 let s = 'I don\'t know';
 let s = "I don\'t know";
 let s = "I don't know";
 let s = '"Hello", he said.';
 let s = "\"Hello\", he said.";
Escaping the escape:
 let s = "1\\2"; s;
       "1\2"
 let s = '\n1\n2\n3\n';
 s
       "
       1
       2
       3
       "
```

--

#### Caractère spéciaux, suite...

```javascript
 let s = '1\r2';
 let s = '1\n\r2';
 let s = '1\r\n2';
// les trois donnent :
 s
       "1
       2"
// tabulation
 let s = "1\t2"
 s
       "1    2"
// Ecrire en bulgare :
 "\u0421\u0442\u043E\u044F\u043D"
       "Стoян"
```