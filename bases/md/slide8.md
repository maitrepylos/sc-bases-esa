##### Fonctions...

###### Une fonction : définition
Une fonction permet de regrouper du code, de lui donner un nom et de pouvoir l'exécuter en l'appelant par son nom.
```javascript
function somme(a, b) {
let c = a + b;
return c;
}
```
Appel de la fonction :
```javascript
 let result = somme(1, 2);
 result;
 ```
3


##### Fonctions et paramètres
Si on omet des paramètres, JavaScript leur donne la valeur undefined :
```javascript
somme(1)
```
NaN

Si on passe trop de paramètres, certains seront ignorés :
```javascript
 somme(1, 2, 3, 4, 5)
```
3


##### Fonctions avec un nombre de paramètres variable
Un tableau de nom arguments est créé automatiquement dans chaque fonction,
Il contient l'ensemble des paramètres d'appel de la fonction :
```javascript
function f() { return arguments; }
f();
[]
f( 1, 2, 3, 4, true, 'ninja');
[1, 2, 3, 4, true, "ninja"]
```


##### Exemple de la somme avec un nombre variable d'arguments
```javascript
function sommeAmelioree() {
let i, res = 0;
let nombreDeParametres = arguments.length;
for (i = 0; i < nombreDeParametres; i++) {
  res += arguments[i];
}

return res;

}

sommeAmelioree(1, 1, 1);
3
sommeAmelioree(1, 2, 3, 4);
10
```


##### Fonctions prédéfinies
Il existe un certain nombre de fonctions prédéfinies en JavaScript
*   parseInt()
*   parseFloat()
*   isNaN()
*   isFinite()
*   encodeURI()
*   decodeURI()
*   encodeURIComponent()
*   decodeURIComponent()
*   eval()


##### parseInt()
Conversion en entier
```javascript
parseInt('123')
123
parseInt('abc123')
NaN
parseInt('1abc23') // attention !
1
parseInt('123abc') // attention !
123
```
Note : il est possible de passer la base en second paramètre...10, 16 ou 8.


##### parseFloat()
Conversion en flottant
```javascript
parseFloat('123')
123
parseFloat('1.23')
1.23
parseFloat('1.23abc.00')
1.23
parseFloat('a.bc1.23')
NaN
parseFloat('123e-2')
1.23
parseFloat('123e2')
12300
parseInt('1e10')
1
```


##### isNaN
Permet de tester si une valeur est NaN
```javascript
isNaN(NaN)
true
isNaN(123)
false
isNaN(1.23)
false
isNaN(parseInt('abc123'))
true
```


##### isFinite()
Vérifie si le paramètre est différent de Infinity ou de NaN
```javascript
isFinite(Infinity)
false
isFinite(-Infinity)
false
isFinite(12)
true
isFinite(1e308)
true
isFinite(1e309) // trop grand !
false
```
Plus grand nombre en JavaScript = 1.7976931348623157e+308 !


###### encodeURI(), encodeURIComponent(), decodeURI(), decodeURIComponent(), escape(), unescape()

Ces fonctions permettent d'échapper certains caractères dans un URI
encodeURI() et son inverse decodeURI() pour les espaces, accents, etc.
```javascript
let url = 'http://www.packtpub.com/scr ipt.php?q=this and that';
encodeURI(url);
// "http://www.packtpub.com/scr%20ipt.php?q=this%20and%20that"
```
encodeURIComponent() et decodeURIComponent() pour les :// etc.
```javascript
encodeURIComponent(url);
//"http%3A%2F%2Fwww.packtpub.com%2Fscr%20ipt.php%3Fq%3Dthis%20and%20that"
```
Ces fonctions remplacent escape() et unescape() qui sont deprecated.


###### eval()
eval() : évaluer du JavaScript passé en paramètre :
```javascript
eval('let i = 2;')
i
2
```
Attention avec eval() :

*   Comment debuger le code évalué ?
*   Performance : il est plus lent d'évaluer du code que de l'exécuter normalement,
*   Sécurité : confiance dans le code évalué ?
*   Pratique pour faire un tutorial JavaScript en ligne !


##### alert()
Fenêtre de dialogue dans le navigateur avec un message.
```javascript
alert("hello!")
```

![](img/alert.jpg)

Bloque le thread de gestion de la GUI du browser. Eviter avec Ajax ! Utiliser la console de debug JavaScript à la place !


##### Portée des variables : attention, différent des autres langages !

JavaScript est très particulier, bien noter ces points :

* Ce qui limite la portée d'une variable c'est la fonction, pas le bloc entre { et }
* Une variable définie dans une fonction ne sera pas visible à l'extérieur de cette fonction,
* Une variable définie dans un if ou un for sera visible à l'extérieur du bloc,
* Variable globale = en dehors d'une fonction,
* Variable locale = dans une fonction
