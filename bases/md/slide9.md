##### Portée des variables : exemples...
```javascript
let global = 1;
function f() {
let local = 2;
global++;
return global;
}
f();
//2
f();
//3
local
//local is not defined
```


##### Le mot clé let, attention au piège !
Une variable non définie avec let est automatiquement considérée comme globale !
![](img/var.jpg)
Lors de la définition de la fonction, local n'existe pas,
Lors du premier appel elle est définie en global, ensuite elle existe... Toujours déclarer avec let !


##### Encore un piège ! Global versus Local
```javascript
let a = 123;
function f() {
alert(a);
let a = 1;
alert(a);
}
f();
```
Le premier alert() affiche Undefined et pas 123 !
En effet, le fait qu'une variable a locale soit définie dans la fonction, même après le alert(), cache le fait qu'il existe un a global, comme le a local n'est pas encore initialisé et déclaré, cela provoque un Undefined !


##### Les fonctions sont des données !
Il s'agit d'un concept très important ! Ces deux écritures sont équivalentes :
```javascript
function f(){return 1;}
let f = function(){return 1;} // déclaration littérale
typeof f
//function"
```
Oui, function est un type de donnée et f une variable de ce type.
Si f est une variable de type fonction on peut exécuter son code an ajoutant (...); à son nom.


##### Les fonctions sont des données : exemple !
```javascript
let somme = function(a, b) {return a + b;}
let addition = somme;
delete somme
//true
typeof somme;
"undefined"
typeof addition;
"function"
addition(1, 2);
3
```
Puisque une variable peut être une fonction, on appliquera les mêmes règles de nommage (débute par un caractère, peut contenir chiffres et underscore).


##### Fonctions anonymes
On peut écrire du code en dehors de fonctions :
```javascript
 "test"; [1,2,3]; undefined; null; 1;
```
On peut aussi définir des fonctions sans nom :
```javascript
function(a){return a;}
```
Intérêt : on peut les passer en paramètres à la manière des écouteurs Java.
En JavaScript ces écouteurs s'appellent des callbacks et ils sont omniprésents dans les applications web.


##### Callbacks !
```javascript
function invoke_and_add(a, b){
return a() + b(); // les paramètres sont des fonctions !
}
function one() {
return 1;
}
function two() {
return 2;
}
invoke_and_add(one, two); // deux fonctions en paramètres !
3
invoke_and_add(function(){return 1;}, function(){return 2;});
3
````
Le dernier exemple utilise des fonctions de callback anonymes (sans noms) !


##### Encore un exemple de callbacks...
Etape 1 : définissons deux fonctions :
```javascript
function multiplieParDeux(a, b, c) {
let i, ar = [];
  for(i = 0; i < 3; i++) {
  ar[i] = arguments[i] * 2;
  }
return ar;
}
function ajouteUn(a) {
return a + 1;
}
 multiplieParDeux(1, 2, 3);
// renvoie un tableau
[2, 4, 6]

 ajouteUn(100)
101
```


##### Suite de l'exemple...
```javascript
let myarr = [];
myarr = multiplieParDeux(10, 20, 30);
[20, 40, 60]
// On ajoute un à chaque élément maintenant :
for (let i = 0; i < 3; i++) {myarr[i] = ajouteUn(myarr[i]);}
myarr
[21, 41, 61]
```

On voudrait tout faire dans la même fonction, mais en pouvant choisir le type d'opération qu'on fait après avoir multiplié tous les éléments par 2 !



##### Suite de l'exemple...
```javascript
function multiplieParDeux(a, b, c, callback) {

let i, ar = [];

for(i = 0; i < 3; i++) {

ar[i] = callback(arguments[i] * 2);

}

return ar;

}

 myarr = multiplieParDeux(1, 2, 3, ajouteUn);

[3, 5, 7]

 myarr = multiplieParDeux(1, 2, 3, function(a){return a + 1});

[3, 5, 7]

 myarr = multiplieParDeux(1, 2, 3, function(a){return a + 2});

[4, 6, 8]
```


###### Fonctions auto-appelantes (self invoking functions)

Attention à la syntaxe !
```javascript
( function(){
    alert('boo');
})();

(function(nom){
    alert('Hello ' + nom + '!');
})('Michel');
```


##### Fonctions auto-appelantes (self invoking functions)
Utile pour une initialisations, puisqu'on ne peut les appeler qu'une fois avec des paramètres prédéfinis.
Evite l'utilisation de variables globales.
Présenté ici car utilisé par certaines librairies, par des closures (étudiées plus loin)...
Font partie des horreurs de JavaScript qui font hurler les puristes. Cool donc ;-)


###### Fonctions "internes" ou privées
```javascript
function a(param1) {

function b(param2) {

return param2 * 2;

};

return 'Le résultat est ' + b(param1);

};
```


Peut s'écrire aussi en forme littérale
```javascript
let a = function(param1) {
let b = function(param2) {
return theinput * 2;
};

return 'Le résultat est ' + b(param1);
};
 a(8);
// "Le résultat est 16"
 b(2);
//b is not defined
```


##### Fonctions internes ou privées : intérêt...
Permet de n'exposer que quelques fonctions, les autres sont "cachées"
Evite les collisions de noms
Très utile quand on fera du javaScript orienté objet (on pourra écrire a.b(); !), une fonction interne sera considérée comme une méthode !


##### Fonctions qui renvoient des fonctions
Les fonctions étant une donnée comme une autre, elle peut être une valeur de retour :
```javascript
function a() {
alert('A!');
return function(){
alert('B!');
};
} 
let f = a();
f(); // fera pparaitre un popup avec 'B' !
a()(); // fait la même chose !
```
a() invoque la fonction a, et a()() invoque la fonction retournée par a()


###### Exemple un peu plus compliqué...
```javascript
let a = function() {

function someSetup(){

let setup = 'done';

}
```


```javascript
function actualWork() {

alert('Worky-worky');

}

someSetup();

return actualWork;

}();
```


Fonction auto invoquée ! a recevra le résultat !
La fonction appelle someSetup() qui peut effectuer des inits ou des tests, par exemple tester dans quel navigateur elle s'exécute...
Elle revoie finalement une fonction (actualWork)... c'est cette valeur que recevra a,
Si on appelle a(); c'est actualWork() qui sera appelée !


###### Les closures
* Les closures sont en général difficiles à comprendre quand on les aborde pour la première fois,
* Ne vous découragez pas ! Un jour comme ça finit par rentrer !
* Nécessite de tester des exemples par vous même, par exemple dans l'IDE en ligne JSbin.com
* Nécessite d'avoir eu des problèmes liés aux closures pour s'y mettre vraiment !
* Avant d'aller plus loin, complètons ce que nous avons vu sur la portée des variables.


##### Rappels sur la portée des variables
Pas de portée par "bloc" mais par "fonction" !
```javascript
let a = 1; function f(){let b = 1; return a;}
f();
1
b
b is not defined
```
Dans f() a et b sont visibles,
En dehors de f() seul a est visible.


###### Cas des fonctions internes
```javascript
let a = 1;
function f(){
let b = 1;
function n() {
let c = 3;
}
}
```
n() est interne à f(),
n() voit les variables de sa portée et aussi celle de son parent. Elle voit donc c, mais aussi b et a.
On parle de "chaîne de portée" ou en anglais "scope chain".


###### Portées "lexicales" : créées à la définition des fonctions
```javascript
function f1(){let a = 1; f2();}
function f2(){return a;}
f1();
a is not defined
```
Dans cet exemple, f2() ne voit que son scope et le scope global, a n'est pas définie dedans.
Pourtant lors de l'appel de f2() par f1(), a est bien définie.


###### Continuons avec les portées de fonctions...
Une portée/scope: ensemble de "chemins" dans lesquels des données seront visibles
Définis à la création, non modifiables, mais on peut ajouter/enlever des données dans ces chemins à l'exécution.
```javascript
function f1(){let a = 1; return f2();}
function f2(){return a;}
f1();
a is not defined
let a = 5;
f1();
5
a = 55;
f1();
55
delete a;
true
f1();
a is not defined
```


###### Exemple précédent, suite...
f2() est dans le scope de f1(), mais on peut modifier la valeur de f2 ou la supprimer.
```javascript
delete f2;
true
f1()
f2 is not defined
let f2 = function(){return a * 2;}
let a = 5;
5
f1();
10
```
Bon, mais et les closures ? Et bien elles servent à "casser" la chaine des portées, la "scope chain" !