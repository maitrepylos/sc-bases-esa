### Trouver le type d'une variable : l'opérateur typeof

typeof : renvoie un objet de stype String, qui peut valoir "number", "string", "boolean", "undefined", "object", ou "function"

```javascript 
let n = 1;
 typeof n;
   "number"
 n = 1234;
 typeof n;
   "number"
 let n2 = 1.23;
 typeof n;
    "number"
 typeof 123;
    "number"
```

--

### Nombres en Octal ou en Hexadécimal

```javascript
let n3 = 0377;
 typeof n3;
    "number"
 n3;
    255
 let  n4 = 0x00;
 typeof n4;
   "number"
 n4;
   0
 let  n5 = 0xff;
 typeof n5;
    "number"
 n5;
    255
```

--

### Nombre avec exposant

```javascript>> 
1e1 10
 1e+1
     10
 2e+3
     2000
 typeof 2e+3;
     "number"
 2e-3
    0.002
 123.456E-3
    0.123456
 typeof 2e-3
  "number"
```

--

### Valeurs infinies

```javascript
 Infinity
   Infinity
 typeof Infinity
   "number"
 1e309
   Infinity
 1e308
   1e+308
 let a = 6 / 0;
 a
     Infinity
 let i = -Infinity;
 i
   -Infinity
 typeof i
   "number"
```

--

### Infinity, suite...

```javascript
Infinity - Infinity
       NaN
 -Infinity + Infinity
       NaN
```

N'importe quelle opération avec Infinity donne Infinity comme résultat.

```javascript
 Infinity - 20
       Infinity
 -Infinity * 3
       -Infinity
 Infinity / 2
       Infinity
 Infinity - 99999999999999999
       Infinity
```

--

### Nan : Not a Number !

NaN est une valeur spéciale et son type est "Number" !

```javascript  
typeof NaN
      "number"
 let
  a = NaN;
 a
      NaN
```

NaN = résultat d'opérations arithmétiques impossibles :

```javascript>> 
let a = 10 * "f";
 a
       NaN
 1 + 2 + a
       NaN
```

