##### Bloc conditionnel, syntaxe alternative

```javascript
let resultat = (a === 1) ? "a vaut 1" : "a est différent de 1";
```

Est équivalent à :
```javascript
let a = 1;

let resultat = '';

if (a === 1) {

resultat = "a vaut 1";

} else {

resultat = "a est différent de 1";

}
```

--

##### Switch... case

Remplace un if avec plusieurs else...
```javascript
let a = '1';

let resultat = '';

switch (a) {

case 1:

resultat = 'Nombre 1';

break;

case '1':

resultat = 'String 1'; // Résultat pour cet exemple

break;

default:

resultat = 'Je ne sais pas';

break;

}

resultat;
```

--

##### Boucles
While... (boucles "tant que...")
```javascript
let i = 0;

while (i < 10) {

i++;

}

```
Do... While (boucles "jusqu'à"...)
```javascript
let i = 0;

do {

i++;

} while (i < 10)
```

--

##### Boucles

For (boucle "pour"...)
```javascript
let punition = '';

for (let i = 0; i < 100; i++) {

punition += 'Je ne copierai plus sur mon voisin,\n';

}
```
```javascript
for (let i = 0, punition = ''; i < 100; i++) {

punition += 'Je ne copierai plus sur mon voisin,\n';

}
```

--

##### Boucles imbriquées
```javascript
let res = '\n';

for(let i = 0; i < 4; i++) {

for(let j = 0; j < 10; j++) {

res += '* ';

}

res+= '\n';

}
```
Affiche :

* * * * * * * * * *

* * * * * * * * * *

* * * * * * * * * *

* * * * * * * * * *

--

##### Boucles for...in
Ces boucles servent à itérer sur les éléments d'un tableau (ou d'un objet, nous verrons cela plus tard)
```javascript
let a = ['a', 'b', 'c', 'x'];

let result = '\n';

for (let i in a) {

result += 'index: ' + i + ', value: ' + a[i] + '\n';

}
```
Résultat :
index: 0, value: a
index: 1, value: b
index: 2, value: c
index: 3, value: x