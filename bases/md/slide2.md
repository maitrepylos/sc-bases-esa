#### Types primitifs, tableaux, boucles, formes conditionnelles...


### Variables

Déclaration, initialisation

```javascript
let ceciEstUneVariable;
let _et_ceci_aussi;
let mix12trois;
// invalide !
let 2three4five; // ne peut commencer par un chiffre !

let a = 1
let v1, v2, v3 = 'hello', v4 = 4, v5;

// Attentions aux majuscules/minuscules
let case_matters = 'lower';
let CASE_MATTERS = 'upper';
```

--

##### Utilisation de la console de debug JavaScript des navigateurs

Très pratiques pour tester la syntaxe JavaScript (F12)

![](img/debug.png)

--

### Operateurs

```javascript
1 + 2
3

let a = 1;
let b = 2;
a + 1
2

b + 2
4

a + b
3

let c = a + b;
c
3
```

--

### Opérateurs classiques

```javascript
1 + 2
   3
99.99 – 11
   88.99
2 * 3
   6
6 / 4
   1.5
```

--

### Modulo
```javascript
6 % 3
  0
5 % 3
  2
4 % 2
    0
5 % 2
    1
```

--

### Pré et post incrément

```javascript
 let a = 123; let b = a++;
 b
   123
 a
   124
 let a = 123; let b = ++a;
 b
   124
 a
   124
 let a = 123; let b = a--;
 b
      123
 a
      122    
```

--

### Ecritures courtes

```javascript
let a = 5;
 a += 3; // équivalent à a = a + 3;
   8
 a -= 3;
     5
 a *= 2;
    10
 a /= 5;
    2
 a %= 2; // équivalent à a = a % 2;
    0
   ```