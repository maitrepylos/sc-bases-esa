##### Booléens

```javascript
let b = true; typeof b;
    "boolean"
let b = false; typeof b;
    "boolean"        
```

Mais... si on met entre quotes :

```javascript 
let b = "true"; typeof b;
"string"
```

--

##### Opérateurs logiques

Ce sont les mêmes qu'en java : && (ET), || (OU), ! (NOT)

```javascript 
let b = !true;
b;
       false
let b = !!true;
b;
       true
let b = "one";
!b;
       false
let b = "one";
!!b;
       true

```

--

##### Conversion implicite de booléens

Dans une expression booléene avec des opérateurs logiques, les valeurs non booléennes sont converties implicitement.

0, null, Undefined, false, NaN, la chaine vide "" sont converties en false

Toute le reste est converti en true

Astuce pour convertir en booléen :

--

##### Précédence des opérateurs

* avant + et -

```javascript 
1 + 2 * 3
   7
1 + (2 * 3)
   7
```

! avant && et ||

```javascript 
false && false || true && true
  true
(false && false) || (true && true) 
  true 
```

Conseil : utilisez des parenthèses et tout ira bien !

--

##### Evaluation fainéante (lazy evaluation)

```javascript 
(false && false) || (true && true)
  true
let b = 5;
true || (b = 6)   // la deuxième partie jamais évaluée !
   true
b
    5
true && (b = 6)   // deuxième partie évaluée
    6
b
    6
```

--

##### Particularité de JavaScript

Si une opérande non booléene est rencontrée dans une expression, c'est cette dernière qui est renvoyée

```javascript 
true || "something" 
    true

true && "something" 
    "something"
```