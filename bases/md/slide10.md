##### Casser la chaine de portée avec une closure.
Imaginons le "global scope" comme l'univers. Il est visible par tous !
![](img/GlobalScope.jpg)
Il contient des variables et des fonctions (les a1, a2) et des fonctions (F)


##### Casser la chaine de portée avec une closure, suite...
Les fonctions ont leur propre scope avec variables et fonctions

![](img/FonctionScope1.jpg)
si F() contient N()...
![](img/FonctionScope2.jpg)



Le point a est dans le scope global, b dans le scope de F, c dans le scope de N, etc.
De a on ne peut voir b, mais de c on peut voir b, donc de N on peut voir b.


##### Closure : mettre N dans le scope global !
... mais sans le sortir réellement !
![](img/closure.jpg)
N est au même endoit que a maintenant,
Il voit encore le scope de F donc il voit b, alors que a ne le voit pas !



##### Comment sortir N de F... sans le sortir réellement ?
Deux possibilités :
* En ne mettant pas le mot clé let devant la définition de la fonction N,
* En faisant en sorte que F retourne N, si F est appelée depuis l'espace global, la valeur retournée (N) devient ainsi visible dans cet espace.

Regardons quelques exemples ensembles...


##### Closure, exemple 1
```javascript
function f(){
let b = "b";
return function(){
return b;
}
}
b
//b is not defined
let n = f();
n();
"b"
```
La fonction retournée est similaire à la fonction N des transparents précédents.


##### Closure, exemple 2
```javascript
let n;
function f(){
let b = "b";
n = function(){ // pas de let !
return b;
}
}
f();
n();
"b"
```
L'appel de f() a défini une nouvelle variable n dans le scope global. Cette variable est une fonction qui voit b.


###### Closure, exemple 3
```javascript
function f(arg) {
let n = function(){
return arg;
};
arg++;
return n;
}
let resultat = f(123);
resultat();
//124
```


arg++ est appelé après la définition de n, pourtant l'appel de resultat() donne bien la valeur à jour incrémentée.
Ceci montre bien que les fonctions sont liées à un scope et pas à des valeurs de variables. Lorsque les variables changent dans le scope, ce sont toujours les valeurs à jour qui sont utilisées.


###### Closures et boucles : le piège archi-classique !
```javascript
function f() {
let a = [];
let i;
for(i = 0; i < 3; i++) {
a[i] = function(){
return i;
}
}
return a;
}
```
Que donne l'exécution de cette fonction ?


###### Reprenons cet exemple et exécutons-le !
```javascript
function f() {
let a = [];
let i;
for(i = 0; i < 3; i++) {
a[i] = function(){
return i;
}
}
return a;
}
let a = f();
a[0]()
3
a[1]()
3
a[2]()
3
```


###### Mais ? Pourquoi ?
Reprenons tranquillement le code en gardant à l'esprit ce que nous venons d'apprendre...
* La fonction f() définit un tableau, et dans une boucle exécutée trois fois, on remplit trois éléments de ce tableau,
* Ce que l'on met dans chaque élément est une fonction anonyme qui renvoit la valeur de i,
* On renvoit le tableau qui contient ces trois fonctions, donc on "sort" à travers ce tableau trois fonctions pour les rendre visible dans l'espace global,
* C'est une Closure !


###### Suite des explications...
* les trois éléments du tableau a sont des fonctions identiques qui voient i puisque i est dans leur scope,
* Si on execute a[0](); ou a[1](); ou a[2](); on exécute le même code,
* Ce code renvoie la valeur de i, la valeur à jour ! Comme f() a déjà été exécutée, i a déjà été incrémenté trois fois, sa valeur à jour est 3, ce qui explique le résultat !


##### Alors , comment faire ?
```javascript
function f() {
let a = [];
let i;
for(i = 0; i < 3; i++) {
a[i] = (function(x){
return function(){
return x;
}
})(i);
}
return a;
}
let a = f();
a[0]();
0
a[1]();
1
a[2]();
2
```


##### Explications...
On met dans a[i] une fonction auto-exécutée :
```javascript
a[i] = (function(x){
return function(){
return x;
}
})(i);
```
Cette fonction prend comme argument i, qui devient le paramètre x, local à la fonction,
a[0] reçoit donc une fonction qui a pour paramètre 0, a[1] une fonction qui a pour paramètre 1, etc.


##### Autre version possible...
```javascript
function f() {
function makeClosure(x) {
return function(){
return x;
}
}
let a = [];
let i;
for(i = 0; i < 3; i++) {
a[i] = makeClosure(i);
}
return a;
}
```
Ca marche ou pas ?


##### Cas classique : un itérateur avec des closures
```javascript
function setup(x) {
let i = 0;
return function(){
return x[i++];
};
}
let next = setup(['a', 'b', 'c']);
next();
"a"
next();
"b"
next();
"c"
```